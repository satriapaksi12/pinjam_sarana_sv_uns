<?php

namespace App\Http\Controllers;

use App\Models\Jenis_acara;
use App\Http\Requests\StoreJenis_acaraRequest;
use App\Http\Requests\UpdateJenis_acaraRequest;

class JenisAcaraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreJenis_acaraRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreJenis_acaraRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Jenis_acara  $jenis_acara
     * @return \Illuminate\Http\Response
     */
    public function show(Jenis_acara $jenis_acara)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Jenis_acara  $jenis_acara
     * @return \Illuminate\Http\Response
     */
    public function edit(Jenis_acara $jenis_acara)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateJenis_acaraRequest  $request
     * @param  \App\Models\Jenis_acara  $jenis_acara
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateJenis_acaraRequest $request, Jenis_acara $jenis_acara)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Jenis_acara  $jenis_acara
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jenis_acara $jenis_acara)
    {
        //
    }
}
