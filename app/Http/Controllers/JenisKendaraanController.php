<?php

namespace App\Http\Controllers;

use App\Models\Jenis_kendaraan;
use App\Http\Requests\StoreJenis_kendaraanRequest;
use App\Http\Requests\UpdateJenis_kendaraanRequest;

class JenisKendaraanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreJenis_kendaraanRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreJenis_kendaraanRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Jenis_kendaraan  $jenis_kendaraan
     * @return \Illuminate\Http\Response
     */
    public function show(Jenis_kendaraan $jenis_kendaraan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Jenis_kendaraan  $jenis_kendaraan
     * @return \Illuminate\Http\Response
     */
    public function edit(Jenis_kendaraan $jenis_kendaraan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateJenis_kendaraanRequest  $request
     * @param  \App\Models\Jenis_kendaraan  $jenis_kendaraan
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateJenis_kendaraanRequest $request, Jenis_kendaraan $jenis_kendaraan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Jenis_kendaraan  $jenis_kendaraan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jenis_kendaraan $jenis_kendaraan)
    {
        //
    }
}
