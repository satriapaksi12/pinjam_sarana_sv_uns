<?php

namespace App\Http\Controllers;

use App\Models\Reservasi_alat;
use App\Http\Requests\StoreReservasi_alatRequest;
use App\Http\Requests\UpdateReservasi_alatRequest;

class ReservasiAlatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreReservasi_alatRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreReservasi_alatRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reservasi_alat  $reservasi_alat
     * @return \Illuminate\Http\Response
     */
    public function show(Reservasi_alat $reservasi_alat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Reservasi_alat  $reservasi_alat
     * @return \Illuminate\Http\Response
     */
    public function edit(Reservasi_alat $reservasi_alat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateReservasi_alatRequest  $request
     * @param  \App\Models\Reservasi_alat  $reservasi_alat
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateReservasi_alatRequest $request, Reservasi_alat $reservasi_alat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reservasi_alat  $reservasi_alat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservasi_alat $reservasi_alat)
    {
        //
    }
}
