<?php

namespace App\Http\Controllers;

use App\Models\Reservasi_kendaraan;
use App\Http\Requests\StoreReservasi_kendaraanRequest;
use App\Http\Requests\UpdateReservasi_kendaraanRequest;

class ReservasiKendaraanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreReservasi_kendaraanRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreReservasi_kendaraanRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reservasi_kendaraan  $reservasi_kendaraan
     * @return \Illuminate\Http\Response
     */
    public function show(Reservasi_kendaraan $reservasi_kendaraan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Reservasi_kendaraan  $reservasi_kendaraan
     * @return \Illuminate\Http\Response
     */
    public function edit(Reservasi_kendaraan $reservasi_kendaraan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateReservasi_kendaraanRequest  $request
     * @param  \App\Models\Reservasi_kendaraan  $reservasi_kendaraan
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateReservasi_kendaraanRequest $request, Reservasi_kendaraan $reservasi_kendaraan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reservasi_kendaraan  $reservasi_kendaraan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservasi_kendaraan $reservasi_kendaraan)
    {
        //
    }
}
