<?php

namespace App\Http\Controllers;

use App\Models\Reservasi_ruang;
use App\Http\Requests\StoreReservasi_ruangRequest;
use App\Http\Requests\UpdateReservasi_ruangRequest;

class ReservasiRuangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreReservasi_ruangRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreReservasi_ruangRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reservasi_ruang  $reservasi_ruang
     * @return \Illuminate\Http\Response
     */
    public function show(Reservasi_ruang $reservasi_ruang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Reservasi_ruang  $reservasi_ruang
     * @return \Illuminate\Http\Response
     */
    public function edit(Reservasi_ruang $reservasi_ruang)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateReservasi_ruangRequest  $request
     * @param  \App\Models\Reservasi_ruang  $reservasi_ruang
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateReservasi_ruangRequest $request, Reservasi_ruang $reservasi_ruang)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reservasi_ruang  $reservasi_ruang
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservasi_ruang $reservasi_ruang)
    {
        //
    }
}
